# Feediverse

Procesa feeds RSS/Atom y los publica como toots en Mastodon.

## Cómo

1. Toma un listado de cuentas

2. Toma el listado de feeds de cada cuenta

2. Los descarga si es necesario (respeta la caché)

3. Los procesa

4. Obtiene todos los artículos del día

5. Elige un artículo y lo envía a la cuenta.  Esto es para evitar enviar
   muchas noticias a la vez y saturar a la gente.  Cada vez que se lo
   corra va a enviar una noticia por usuaria configurada.

## Planes

* Convertir en un servicio web donde la gente pueda autorizar sus
  cuentas para agregar feeds?

## Instalación

Instalar redis:

```
# con pacman
pacman -Sy redis
# iniciar el servicio
systemctl restart redis
```

```
# con apt
apt upgrade ; apt install redis-server
# iniciar el servicio
systemctl restart redis-server
```

Instalar gemas:

```
bundle install --path=~/.gem
```

Configurar:

```
cp config.yml{.example,}
```

Registrar la app en la cuenta de Mastodon, yendo a `Preferencias >
Desarrollo > Registrar app` y obtener el `token`.  Necesitamos un
`token` por cuenta.

Luego, configurar una lista de feeds que queramos publicar desde esa
cuenta.

Correr:

```
bundle exec feediverse
```

Poner en cronjob:

```
./bin/whenever --update
```
