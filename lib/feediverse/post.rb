module Feediverse
  class Post < Model
    attribute :url
    unique :url
    index :url
    attribute :title
    attribute :summary
    attribute :image
    attribute :sent
    index :sent
    attribute :feed
    index :feed
    attribute :identity
    index :identity
  end
end
