module Feediverse
  class Chat < Model
    attribute :name
    index :name
    unique :name
  end
end
