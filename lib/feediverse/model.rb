module Feediverse
  class Model < Ohm::Model
    def self.find_by(*attributes)
      self.find(*attributes).to_a.first
    end

    def self.find_or_create_by(*attributes)
      self.find_by(*attributes) || self.create(*attributes)
    end
  end
end
