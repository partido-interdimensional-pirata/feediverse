set :application, 'feediverse'
set :repo_url, 'https://0xacab.org/partido-interdimensional-pirata/feediverse'
set :linked_files, %w{config.yml}
set :linked_dirs, %w{}

set :rbenv_type, :user
set :rbenv_ruby, '2.5.0'

set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value

set :bundle_flags, '--deployment'
