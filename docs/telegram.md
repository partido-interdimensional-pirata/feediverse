# Telegram

## Registrar unx cyborg

* Abrir una conversación con @BotFather

* Decirle `/newbot`

* Va a preguntar el nombre, se lo decimos a continuación

* Va a pedir el alias, tiene que terminar en `Bot` o `_bot`, por ejemplo
  `FeediverseBot` o `feediverse_bot`.

* Nos va a dar un token, que es un código estilo contraseña que vamos a
  usar para enviar mensajes desde feediverse.

* Para agregarle una foto, hay que decirle `/setuserpic`

* Nos va a pedir que elijamos a qué cyborg queremos agregarle foto,
  decimos el alias, ej. `@FeediverseBot`

* Nos va a pedir la foto, se la enviamos como imagen.

## Agregarle a un canal

* Las administradoras del canal tienen que agregar a lx cyborg como
  administradora.  No es posible agregar cyborgs como usuarias normales.

* Ir a la administración del canal y agregar una administradora nueva

* Buscar el `@alias` de lx cyborg

* Quitarle todos los permisos salvo publicar mensajes.  Esto es por
  seguridad, si alguien nos quita el token, el único daño que puede
  hacer es publicar mensajes.

## Configurarlo en Feediverse

* Agregar el token y el username a la configuración, junto con las otras
  opciones:

```yaml
  edsl:
    telegram:
      token: ElTokenQueNosDioBotFather
      username: NombreDeLxBot
```
